#define F_CPU 1000000L
#include <avr/io.h>
#include <util/delay.h>


int main(void) {
    DDRC = 0x01;
	while (1) {
		PORTC = 0x01;
		_delay_ms(500);
		PORTC = 0x00;
		_delay_ms(500);
    }
}

