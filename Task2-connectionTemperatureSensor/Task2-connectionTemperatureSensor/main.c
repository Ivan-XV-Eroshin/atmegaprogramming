# define F_CPU 1000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>

#define THERMO_DDR DDRD
#define THERMO_PORT PORTD
#define THERMO_PIN PIND1
#define DS18B20 1

#define SKIP_ROM 0xCC
#define READ_ROM 0x33
#define SEARCH_ROM 0xF0
#define MATCH_ROM 0x55
#define CONVERT_T 0X44
#define WRITE_RAM 0x4E
#define READ_RAM 0xBE
#define COPY_RAM_TO_EEPROM 0x48
#define COPY_RAM_FROM_EEPROM 0xB8

void sendSerialCommand(char command, const char beginFromLowBit){
	if (beginFromLowBit) {
		for (char i = 0; i<8; i++) {
			THERMO_DDR |= (1<<DS18B20);
			THERMO_PORT &=~(1<<DS18B20);
			if (command%2 != 0) {
				//запись 1 по таймингам
				_delay_us(14);
				THERMO_DDR &=~(1<<DS18B20);
				_delay_us(50);
			}
			else {
				//запись 0 по таймингам;
				_delay_us(59);
				THERMO_DDR &=~(1<<DS18B20);
			}
			command>>=1;
		}
	}
}

bool resetPrecenseSignal() {
	THERMO_DDR |= (1<<DS18B20);
	THERMO_PORT &= ~(1<<DS18B20);
	_delay_us(480);
	THERMO_DDR &= ~(1<<DS18B20);
	_delay_us(60);
	for (char i = 0; i < 180; i++) {
		if(THERMO_PIN & ~(1<<DS18B20)){
			_delay_us(420);
			return true;	
		}
	}
	return false;
}

void generateReadSlot() {
	THERMO_DDR |= (1<<DS18B20);
	THERMO_PORT &= ~(1<<DS18B20);
	THERMO_DDR &= ~(1<<DS18B20);
}

uint8_t readByteOnWire(){
	uint8_t resultByte = 0;
	for (uint8_t bit = 0; bit < 8; bit ++) {
		generateReadSlot();
		_delay_us(11);
		uint8_t data = PIND;
		if (data & (1 << DS18B20))
			resultByte |= (1 << bit);
		_delay_us(46);
	}
	return resultByte;
}

int main(void) {
		DDRB = 0xFF;
		resetPrecenseSignal();
		sendSerialCommand(SKIP_ROM, true);
		sendSerialCommand(READ_RAM, true);
		THERMO_DDR &= ~(1<<DS18B20);
		int resultByte;
		readByteOnWire();
		resultByte = readByteOnWire();
		PORTB = resultByte;
}
